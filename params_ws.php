<?php

	$GLOBALS['PARAM_WS']                                           	=   array();

	$GLOBALS['PARAM_WS']['LIB_STATUS']	                           	=	'se_status';
	$GLOBALS['PARAM_WS']['LIB_MESSAGE']	            				=	'se_message';
	$GLOBALS['PARAM_WS']['LIB_DATA']	               				=	'se_data';
	$GLOBALS['PARAM_WS']['STATUS_OK']	                       		=	200;
	$GLOBALS['PARAM_WS']['STATUS_KO']	                       		=	400;
    
	$GLOBALS['PARAM_WS']['LOGIN_OK']	                        	=	'log_ok';
	$GLOBALS['PARAM_WS']['LOGIN_KO']	            				=	'log_ko';
	$GLOBALS['PARAM_WS']['OK']	            						=	'ok';
	$GLOBALS['PARAM_WS']['KO']	            						=	'ko';
?>