<?php session_start();
require_once 'common_ws.php';

require_once $GLOBALS['SISED_PATH_LIB'] . 'adodb_xml/class.ADODB_XML.php';

$app = new \Slim\Slim();

$lib_status =  $GLOBALS['PARAM_WS']['LIB_STATUS'];
$lib_message = $GLOBALS['PARAM_WS']['LIB_MESSAGE'];
$lib_data = $GLOBALS['PARAM_WS']['LIB_DATA'];

$status_ok = $GLOBALS['PARAM_WS']['STATUS_OK'];
$status_ko = $GLOBALS['PARAM_WS']['STATUS_KO'];

$app->add(new \HttpAuth());

 // cherche les nouvelles campagnes disponibles pour un utilisateur
$app->get('/new_camp/:user_id/:id_period', function ($user_id, $id_period) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];
	
	$camp_list = array();
	$id_year = $_SESSION['annee'];
	$requete = "SELECT DISTINCT ID_CAMPAGNE, ID_TYPE_REGROUP, ID_TYPE_REGROUP_PARENTS
				FROM DICO_FIXE_REGROUPEMENT
				WHERE ID_USER=".$user_id."
        		AND ID_ANNEE=".$id_year."   
        		AND ID_PERIODE=".$id_period." 
				ORDER BY ID_CAMPAGNE;";
  
	$camps    = $GLOBALS['conn_dico']->GetAll($requete);
	
	$id_camps = array();
	$type_regroups = array();
	
	if (count($camps) > 0) {
		foreach ($camps as $row) {
			$id_camp = $row["ID_CAMPAGNE"];
			$type_regroups[$id_camp] = array();
			if (trim($row["ID_TYPE_REGROUP_PARENTS"]) != '') {
				$reg_pars = preg_split("/,/", $row["ID_TYPE_REGROUP_PARENTS"]);
			}
			$reg = $row["ID_TYPE_REGROUP"];
			if (($reg != 0) && !in_array($reg, $type_regroups[$id_camp])) {
				$type_regroups[$id_camp][] = $reg;
				$reg_p_children = get_type_reg_id($reg);
				foreach ($reg_p_children as $child) {
					if (!in_array($child['id'], $type_regroups[$id_camp])) {
						$type_regroups[$id_camp][] = $child['id'];
					}
				}
			}
			foreach ($reg_pars as $reg_par) {
				if (!in_array($reg_par, $type_regroups[$id_camp])) {
					$type_regroups[$id_camp][] = $reg_par;
				}
			}
			if (!in_array($id_camp, $id_camps)) {
				$id_camps[] = $id_camp;
			}
			asort($type_regroups[$id_camp]);
		}
	}

	if (count($id_camps) > 0) {
		foreach ($id_camps as $id_camp) {
			$requete = "SELECT ".$GLOBALS['PARAM']['TYPE_RATTACHEMENT'].".*
						FROM ".$GLOBALS['PARAM']['TYPE_RATTACHEMENT']."
						WHERE ".$GLOBALS['PARAM']['TYPE_RATTACHEMENT'].".".$GLOBALS['PARAM']['CODE']."_".$GLOBALS['PARAM']['TYPE_RATTACHEMENT']."=".$id_camp.";";

			$camp = $GLOBALS['conn']->GetAll($requete);
			$type_regroups[$id_camp] = array_values($type_regroups[$id_camp]);

			$camp_list[] = array("id"=>$id_camp, "nom"=>htmlEncode(trim($camp[0][$GLOBALS['PARAM']['LIBELLE']."_".$GLOBALS['PARAM']['TYPE_RATTACHEMENT']])), "debut"=>"", "fin"=>"", "statut"=>2, "typeregroups" =>implode(",", $type_regroups[$id_camp]));
		}
	}
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$camp_list);
	//recherche les nouvelles campagnes pour un utilisateur
	echo json_encode($rps);
});

// cherche les syst�mes concern�s par une campagne donn�es pour un utilisateur
$app->get('/sys_camp/:user_id/:id_camp', function ($user_id, $id_camp) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];
	$sys_list = array();
	
	$requete = "SELECT DISTINCT SYSTEME.ID_SYSTEME, SYSTEME.LIBELLE_SYSTEME
				FROM DICO_FIXE_REGROUPEMENT INNER JOIN SYSTEME ON DICO_FIXE_REGROUPEMENT.ID_SYSTEME = SYSTEME.ID_SYSTEME
				WHERE (((DICO_FIXE_REGROUPEMENT.ID_USER)=".$user_id.") AND ((DICO_FIXE_REGROUPEMENT.ID_CAMPAGNE)=".$id_camp."));";
   
	$systems = $GLOBALS['conn_dico']->GetAll($requete);
	
	foreach ($systems as $sys) {
		$sys_list[] = array("id"=>$sys["ID_SYSTEME"], "nom"=>htmlEncode($sys["LIBELLE_SYSTEME"]));
	}
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$sys_list);
	// recherche les systemes
	echo json_encode($rps);
});

// cherche les regroupements concern�s par une campagne donn�es pour un utilisateur
$app->get('/reg_camp/:user_login/:id_camp/:id_period', function ($user_login, $id_camp, $id_period) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko, $arbre) {
	$status = $GLOBALS['PARAM_WS']['OK'];
	$reg_list = array();
	
	$sql = "SELECT CODE_USER FROM ADMIN_USERS WHERE NOM_USER='$user_login'";
	$user_id = $GLOBALS['conn_dico']->GetOne($sql);  
	
	if (!$user_id || $user_id == '') {
		sendError("Utilisateur non trouv&eacute;"); 
		return;
	}  
	                
	$id_year = $_SESSION['annee'];
	$requete = "SELECT ID_CAMPAGNE, ID_CHAINE, ID_TYPE_REGROUP, ID_REGROUP, ID_REGROUP_PARENTS, ID_TYPE_REGROUP_PARENTS
				FROM DICO_FIXE_REGROUPEMENT
				WHERE ID_USER=".$user_id." AND ID_CAMPAGNE=".$id_camp." AND ID_ANNEE=".$id_year." AND ID_PERIODE=".$id_period.";";
    //echo $requete."<br/>"; 
	$camps = $GLOBALS['conn_dico']->GetAll($requete);
	
	$regroups = array();
	
	if (count($camps) > 0) {
		foreach ($camps as $row) {
			$ch = $row["ID_CHAINE"];
			$arbre = new arbre($ch);
			if ($regroups[$ch] == NULL) {
				$regroups[$ch] = array();
			}
			$reg_pars = preg_split("/,/", $row["ID_REGROUP_PARENTS"]);
			$type_reg_pars = preg_split("/,/", $row["ID_TYPE_REGROUP_PARENTS"]);
			$type_reg = $row["ID_TYPE_REGROUP"];
			$regs = preg_split("/,/", $row["ID_REGROUP"]);
			if ($type_reg != 0) {
				foreach ($regs as $reg) {
          			$niv_profond = $arbre->get_depht_regroup($reg);
					$reg_childs = $arbre->getchildsid($niv_profond, $reg, $ch);//print_r($reg_childs); echo "<br/><br/>";
					//$reg_childs = array_values($reg_childs[0]); 
					if (count($reg_childs) == 1) {
						$reg_childs[0][$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT']] = $row["ID_TYPE_REGROUP"];
						$regroups[$ch][] = $reg_childs[0];
					} else {
						$regroups[$ch] = array_merge($regroups[$ch], $reg_childs);
						$regroups[$ch][] = array($GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT']=>$type_reg, $GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']=>$reg);
					}
				}
			}
			$i = 0;
			foreach ($reg_pars as $reg_par) {
				$parent = array($GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']=>$reg_par, $GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT']=>$type_reg_pars[$i]);
				if (!in_array($parent, $regroups[$ch])) {
					$regroups[$ch][] = $parent;
				}
				$i++;
			}
		}
	}
	
	foreach ($regroups as $ch=>$regs) {
		$arbre = new arbre($ch);
		foreach ($regs as $reg) {
      		$niv_profond = $arbre->get_depht_regroup($reg[$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']]);
			$reg_c = $arbre->getregwithparentid($niv_profond, $reg[$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']]);
			if (!in_array($reg_c, $reg_list) && $reg_c) {
				$reg_list[] = $reg_c;
			}
		}
	}

	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$reg_list);
	// recherche les regroupements
	echo json_encode($rps);
});

// cherche les types de regroupements concern�s par une campagne donn�es pour un utilisateur
$app->get('/typ_reg_camp/:user_id/:id_camp/:id_types_reg', function ($user_id, $id_camp, $id_types_reg) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];
	$type_reg_list = array();	
	
	$reg_types = preg_split("/,/", $id_types_reg);
	foreach ($reg_types as $type) {
		$type_reg_list[] = get_type_reg_data($type);
	}
	
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$type_reg_list);
	// recherche les types de regroupements
	echo json_encode($rps);
});

// renvoie les differents statuts existants
$app->get('/etabs_status/', function () use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];	
	$etab_status = array();

	$requete = "SELECT ".$GLOBALS['PARAM']['CODE']."_".$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT']." AS id, ".$GLOBALS['PARAM']['LIBELLE']."_".$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT']." AS name
				FROM ".$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT'].";";
	
	$etab_statuts = $GLOBALS['conn']->GetAll($requete);
	
	for($i=0;$i<count($etab_statuts);$i++) {
		$etab_statuts[$i]['name'] = htmlEncode($etab_statuts[$i]['name']);
	}
				
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$etab_statuts);
	
	echo json_encode($rps);
});

// cherche les �tablissements concern�s par une campagne donn�es pour un utilisateur
$app->get('/etabs_camp/:user_id/:id_camp/:id_period', function ($user_id, $id_camp, $id_period) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];	
	$etab_list = array();  
	$id_year = $_SESSION['annee'];
	
	$requete = "SELECT ID_CHAINE, ID_SYSTEME, ID_TYPE_REGROUP, ID_REGROUP
				FROM DICO_FIXE_REGROUPEMENT
				WHERE ID_USER=".$user_id." AND ID_CAMPAGNE=".$id_camp." AND ID_ANNEE=".$id_year." AND ID_PERIODE=".$id_period.";";
	
	$regroups = $GLOBALS['conn_dico']->GetAll($requete);
		
	$with_code_admin =  '';
  	$with_status = '';
	if( exist_champ_in_table($GLOBALS['PARAM']['CODE_ADMINISTRATIF'], $GLOBALS['PARAM']['ETABLISSEMENT']) && ($GLOBALS['PARAM']['CONCAT_CODE_ADMIN']) ){
		$with_code_admin = ', E.'.$GLOBALS['PARAM']['CODE_ADMINISTRATIF'].' AS code ' ;
	}
  	if( exist_champ_in_table($GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT'], $GLOBALS['PARAM']['ETABLISSEMENT'])){
		$with_status = ', E.'.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT'].' AS status ' ;
	}
	
	if (count($regroups) > 0) {
		foreach ($regroups as $row) {
			$ch = $row["ID_CHAINE"];
			$sys = $row["ID_SYSTEME"];
			$arbre = new arbre($ch);
			$type_reg = $row["ID_TYPE_REGROUP"];
			$regs = preg_split("/,/", $row["ID_REGROUP"]);
			if ($type_reg == 0) {
				foreach ($regs as $etab_id) { 
					$requete_etab   = 'SELECT E_R.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].' AS id, E_R.'.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT'].' AS idregroup, E.'.$GLOBALS['PARAM']['NOM_ETABLISSEMENT'].' AS nom'. $with_status . $with_code_admin .'
									 FROM '.$GLOBALS['PARAM']['ETABLISSEMENT'].' AS E, '.$GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT'].' AS E_R'.$from_periode.'  
									 WHERE E.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].' = E_R.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].$where_periode.'
									 AND E.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].'='.$etab_id.';';
					//echo $requete_etab;
          $etab = $GLOBALS['conn']->GetAll($requete_etab);
          $etab = $etab[0];
					if (!in_array($etab, $etab_list) && $etab != null) {
						$etab['nom'] = htmlEncode($etab['nom']);
						$etab_list[] = $etab;                                       
					}
				}
			} else {
				foreach ($regs as $reg) {
          			$niv_profond = $arbre->get_depht_regroup($reg);
					$etabs = $arbre->get_list_etabs($niv_profond, $reg, $sys, $ch, $id_camp, $id_year, $id_period);
          			//print_r($regroups);echo "<br/><br/><br/>";
					foreach ($etabs as $etab) { 
						if (!in_array($etab, $etab_list) && $etab != null) {
							$etab['nom'] = htmlEncode($etab['nom']);
							$etab_list[] = $etab;
						}
					}
				}
			}
		}
	}
	
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$etab_list);
	// recherche les �tablissements
	echo json_encode($rps);
});

// cherche les �tablissements concern�s par une campagne donn�es pour un utilisateur
$app->get('/etabs_camp_zip/:user_login/:id_camp/:id_period', function ($user_login, $id_camp, $id_period) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko, $app) {
	$status = $GLOBALS['PARAM_WS']['OK'];	
	$etab_list = array();  
	$id_year = $_SESSION['annee'];
	$user_id = $GLOBALS['conn_dico']->GetOne("SELECT CODE_USER FROM ADMIN_USERS WHERE NOM_USER='$user_login'");
	$requete = "SELECT ID_CHAINE, ID_SYSTEME, ID_TYPE_REGROUP, ID_REGROUP
				FROM DICO_FIXE_REGROUPEMENT
				WHERE ID_USER=".$user_id." AND ID_CAMPAGNE=".$id_camp." AND ID_ANNEE=".$id_year." AND ID_PERIODE=".$id_period.";";
	
	$regroups = $GLOBALS['conn_dico']->GetAll($requete);
		
	$with_code_admin =  '';
  	$with_status = '';
	if( exist_champ_in_table($GLOBALS['PARAM']['CODE_ADMINISTRATIF'], $GLOBALS['PARAM']['ETABLISSEMENT']) && ($GLOBALS['PARAM']['CONCAT_CODE_ADMIN']) ){
		$with_code_admin = ', E.'.$GLOBALS['PARAM']['CODE_ADMINISTRATIF'].' AS code ' ;
	}
  	if( exist_champ_in_table($GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT'], $GLOBALS['PARAM']['ETABLISSEMENT'])){
		$with_status = ', E.'.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_STATUT_ETABLISSEMENT'].' AS status ' ;
	}
	
	if (count($regroups) > 0) {
		foreach ($regroups as $row) {
			$ch = $row["ID_CHAINE"];
			$sys = $row["ID_SYSTEME"];
			$arbre = new arbre($ch);
			$type_reg = $row["ID_TYPE_REGROUP"];
			$regs = preg_split("/,/", $row["ID_REGROUP"]);
			if ($type_reg == 0) {
				foreach ($regs as $etab_id) { 
					$requete_etab   = 'SELECT E_R.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].' AS id, E_R.'.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT'].' AS idregroup, E.'.$GLOBALS['PARAM']['NOM_ETABLISSEMENT'].' AS nom'. $with_status . $with_code_admin .'
									 FROM '.$GLOBALS['PARAM']['ETABLISSEMENT'].' AS E, '.$GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT'].' AS E_R'.$from_periode.'  
									 WHERE E.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].' = E_R.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].$where_periode.'
									 AND E.'.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].'='.$etab_id.';';
					//echo $requete_etab;
          $etab = $GLOBALS['conn']->GetAll($requete_etab);
          $etab = $etab[0];
					if (!in_array($etab, $etab_list) && $etab != null) {
						$etab_list[] = $etab;                                       
					}
				}
			} else {
				foreach ($regs as $reg) {
          			$niv_profond = $arbre->get_depht_regroup($reg);
					$etabs = $arbre->get_list_etabs($niv_profond, $reg, $sys, $ch, $id_camp, $id_year, $id_period);
          			//print_r($regroups);echo "<br/>";
					foreach ($etabs as $etab) { 
						if (!in_array($etab, $etab_list) && $etab != null) {
							$etab_list[] = $etab;
						}
					}
				}
			}
		}
	}
	
	$idSchools = array();
	$idRegs = array();
	foreach($etab_list as $etab) {
		$idSchools[] = $etab['id'];
		$idRegs[] = $etab['idregroup'];
	}
	
	$sqlSchools = "SELECT * FROM ".$GLOBALS['PARAM']['ETABLISSEMENT']." WHERE ".$GLOBALS['PARAM']['CODE_ETABLISSEMENT']." IN (".join(', ',$idSchools).")";
	$sqlSchoolsReg = "SELECT * FROM ".$GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT']." WHERE ".$GLOBALS['PARAM']['CODE_ETABLISSEMENT']." IN (".join(', ',$idSchools).") AND ".$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']." IN (".join(', ',$idRegs).")";
	
	$rootDir = time();
	$exportDir = $GLOBALS['SISED_PATH']."server-side/import_export/".$rootDir;
	if (!file_exists($exportDir)) {
		mkdir($exportDir, 0777, true);
	}
	$adodbXML = new ADODB_XML("1.0", "ISO-8859-1");
	$all_champs_table = $GLOBALS['conn']->MetaColumns($GLOBALS['PARAM']['ETABLISSEMENT']);
	$schoolFields = array();
	foreach( $all_champs_table as $champ ) {
		$schoolFields[] = $champ->name;
	}	
	$all_champs_table = $GLOBALS['conn']->MetaColumns($GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT']);
	$schoolRegFields = array();
	foreach( $all_champs_table as $champ ) {
		$schoolRegFields[] = $champ->name;
	}	  
	$adodbXML->ConvertToXML($GLOBALS['conn'], $sqlSchools, $schoolFields, $exportDir."/".$GLOBALS['PARAM']['ETABLISSEMENT'].".xml");	
	$adodbXML->ConvertToXML($GLOBALS['conn'], $sqlSchoolsReg, $schoolRegFields, $exportDir."/".$GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT'].".xml");	
	if (create_zip($GLOBALS['SISED_PATH']."server-side/import_export", $rootDir)) {
		$fData = file_get_contents($GLOBALS['SISED_PATH']."server-side/import_export/".$rootDir.".zip");
		$app->response->header('Content-Type', 'application/octet-stream');
		$app->response->header('Pragma', "public");
		$app->response->header('Content-disposition:', 'attachment; filename='. $rootDir.'.zip');
		$app->response->header('Content-Transfer-Encoding', 'binary');
		$app->response->header("Content-Description", "File Transfer");
		$app->response->header('Content-Length', filesize($GLOBALS['SISED_PATH']."server-side/import_export/".$rootDir.".zip"));
		$app->response->setBody($fData);		
	} else {	
		$rps = array($lib_status=>$status_ko,$lib_message=>$status_ko,$lib_data=>'');
		// recherche les �tablissements
		echo json_encode($rps);
	}
});

// cherche les chaines de localisation concern�s par une campagne donn�es pour un utilisateur
$app->get('/locs_camp/:user_id/:id_camp', function ($user_id, $id_camp) use ($lib_status, $lib_message, $lib_data, $status_ok, $status_ko) {
	$status = $GLOBALS['PARAM_WS']['OK'];		
	$loc_list = array();
	
	$requete = "SELECT ID_CAMPAGNE, ID_SYSTEME, ID_CHAINE, ID_TYPE_REGROUP, ID_REGROUP, ID_REGROUP_PARENTS, ID_TYPE_REGROUP_PARENTS
				FROM DICO_FIXE_REGROUPEMENT
				WHERE ID_USER=".$user_id." AND ID_CAMPAGNE=".$id_camp.";";
   
	$camps    = $GLOBALS['conn_dico']->GetAll($requete);
	
	if (count($camps) > 0) {
		foreach ($camps as $row) {
			$ch = $row["ID_CHAINE"];
			$sys = $row["ID_SYSTEME"];
			$arbre = new arbre($ch);
			$regroups = array();
			$etabs = array();
			$reg_pars = preg_split("/,/", $row["ID_REGROUP_PARENTS"]);
			$type_reg = $row["ID_TYPE_REGROUP"];
			$regs = preg_split("/,/", $row["ID_REGROUP"]);
			if ($type_reg == 0) {
				$etabs = array_merge($etabs, $regs);
			} else {
				foreach ($regs as $reg) {
					$niv_profond = $arbre->get_depht_regroup($reg);
					$reg_childs = $arbre->getchildsid($niv_profond, $reg, $ch);//print_r($reg_childs); echo $reg."<br/><br/>";
					//$reg_childs = array_values($reg_childs[0]); 
					if (count($reg_childs) == 1) {
						if (!in_array($reg_childs[0][$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']], $regroups)) {
							$regroups[] = $reg_childs[0][$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']];
						}
					} else {
						foreach ($reg_childs as $reg_id) { 
							if (!in_array($reg_id[$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']], $regroups)) {
								$regroups[] = $reg_id[$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['REGROUPEMENT']];
							}
						}
						if (!in_array($reg, $regroups)) {
							$regroups[] = $reg;
						}
					}
          			$niv_profond = $arbre->get_depht_regroup($reg);
					$curr_etabs = $arbre->get_list_etabs_ids($niv_profond, $reg, $sys);
					foreach ($curr_etabs as $curr_etab) {
						if (!in_array($curr_etab['id'], $etabs)) {
							$etabs[] = $curr_etab['id'];
						}
					}
				}
			}
			foreach ($reg_pars as $reg_par) {
				if (!in_array($reg_par, $regroups)) {
					$regroups[] = $reg_par;
				}
			}
			$loc_list[] = array("idloc"=>(int)($user_id.$id_camp.$sys), "idcamp"=>$id_camp, "idsys"=>$sys, "regroups"=>implode(",", $regroups), "etabs"=>implode(",", $etabs));
		}
	}
	
	$rps = array($lib_status=>$status_ok,$lib_message=>$status,$lib_data=>$loc_list);
	// recherche les chaines de localisation
	echo json_encode($rps);
});

function get_type_reg_data($code_type_reg) {
	$requete = 'SELECT '.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' AS id, '.$GLOBALS['PARAM']['LIBELLE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' AS nom, '.$GLOBALS['PARAM']['ORDRE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' AS ordre FROM '.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' WHERE '.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].'='.$code_type_reg.';';            
	
	$result = $GLOBALS['conn']->GetAll($requete);
	$result[0]['nom'] = htmlEncode($result[0]['nom']);
	return($result[0]);
}

function get_type_reg_id($code_type_reg) {
	$requete = 'SELECT '.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' AS id FROM '.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].' WHERE '.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_REGROUPEMENT'].'>'.$code_type_reg.';';            
	
	$result = $GLOBALS['conn']->GetAll($requete);
	return($result);
}

function comp_reg($reg1, $reg2) {  
  return strcasecmp($reg1['nom'], $reg2['nom']);
}

$app->run();
 
function sendError($message) {
	$posts = array('se_statut'=>101,'se_message'=>$message,'se_data'=>NULL);	
	echo json_encode($posts);
}
?>
