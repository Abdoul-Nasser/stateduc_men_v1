<?php
require_once 'config_app.php';
require_once 'params.php';
require_once 'params_sys.php';
require_once 'params_ws.php';
include 'constants.php';

require_once $GLOBALS['SISED_PATH_LIB'] . 'fonctions.inc.php';
//include slim
require $GLOBALS['SISED_PATH_LIB'] . 'codeguy-Slim/Slim/Slim.php';
require $GLOBALS['SISED_PATH_LIB'] . 'codeguy-Slim/Slim/Middleware.php';
require $GLOBALS['SISED_PATH_INC'] . 'web_services/HttpAuth.php';
\Slim\Slim::registerAutoloader();
//include slim
require_once $GLOBALS['SISED_PATH_CLS'] . 'adodb/adodb.inc.php';
manage_magic_quotes();
set_time_limit(0);
ini_set("memory_limit", "64M");

//Variable ADODB
$ADODB_FETCH_MODE   = ADODB_FETCH_ASSOC;
$ADODB_CACHE_DIR    = $SISED_PATH . 'server-side/adodbcache/';

if (file_exists($GLOBALS['SISED_PATH'] . 'server-side/dico_DB.mdb')) {
	//require_once $GLOBALS['SISED_PATH_CLS'] . 'adodb/adodb.inc.php';
	$conn_dico = ADONewConnection('access');
	$conn_dico->Connect('Driver={Microsoft Access Driver (*.mdb)};Dbq=' . $GLOBALS['SISED_PATH'] . 'server-side/dico_DB.mdb' . ';Uid=Admin;Pwd=\'\';');
	$GLOBALS['conn_dico'] = $conn_dico;
}

require_once $GLOBALS['SISED_PATH_CLS'] . 'connexion.class.php';

$source = false;
$connexion = new connexion();
$connexion->init($source);

require_once $GLOBALS['SISED_PATH_CLS'] . 'arbre/arbre.class.php';

session_start();
$requete = "SELECT * FROM PARAM_DEFAUT;";
$params = $GLOBALS['conn_dico']->GetRow($requete);
if(!isset($_SESSION['langue']) || preg_match('#index.php#',$_SERVER['PHP_SELF'])) {
		$_SESSION['langue'] = trim($params['CODE_LANGUE']);
		//set_tab_session('secteurs', $_SESSION['langue']);
} else {
		if(isset($_GET['langue']) && $_GET['langue'] != $_SESSION['langue']) {
			$_SESSION['langue'] = $_GET['langue'];
		}
}
if(!isset($_SESSION['secteur']) || preg_match('#index.php#',$_SERVER['PHP_SELF'])) {
		$_SESSION['secteur']    =   trim($params['CODE_SECTEUR']);
		//set_tab_session('secteurs', $_SESSION['langue']);
} else {
		//Il faut limiter l'utilisateur � ses secteurs en cas de limitation d'acces activ�e
		if(isset($_SESSION['fixe_secteurs']) && count($_SESSION['fixe_secteurs'])>0){
			if(!in_array($_SESSION['secteur'],$_SESSION['fixe_secteurs'])){
				$_SESSION['secteur'] = $_SESSION['tab_secteur'][0][$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_SYSTEME_ENSEIGNEMENT']];
			}
		}

}
set_tab_session('secteurs', $_SESSION['langue']);
// Les chaines de regroupement
set_tab_session('chaines', '');                
// Les ann�es
set_tab_session('annees', ''); 
// Les filtres
set_tab_session('filtres', ''); 
// Les langues
set_tab_session('langues', ''); 
// Les p�riodes
if(isset($GLOBALS['PARAM']['TYPE_PERIODE']) && $GLOBALS['PARAM']['TYPE_PERIODE'] <> '')
	set_tab_session('periodes', ''); 
/**
* Param�tres
*
*/
$_SESSION['NB_NIVEAU_ARBO'] = $params['NB_NIVEAU_ARBO'];
$_SESSION['ARMOIRIES_PAYS'] = $params['ARMOIRIES_PAYS'];
$_SESSION['DRAPEAU_PAYS'] = $params['DRAPEAU_PAYS'];
$_SESSION['NOM_PAYS'] = $params['NOM_PAYS'];
// Rechargement des libell�s pages
$requete   = "SELECT LIBELLE, CODE_LIBELLE
				FROM DICO_LIBELLE_PAGE 
				WHERE CODE_LANGUE='".$_SESSION['langue']."';";
$_SESSION['tab_libelles'] = $GLOBALS['conn_dico']->GetAll($requete);
//set_tab_session('secteurs', $_SESSION['langue']);
//gestion de l'ann�e
if(!isset($_SESSION['annee']) || preg_match('#index.php#',$_SERVER['PHP_SELF'])) {
		$_SESSION['annee']    =   trim($params['CODE_ANNEE']);
} else {
		if(isset($_GET['annee']) && $_GET['annee'] != $_SESSION['annee']) {
				$_SESSION['annee'] = $_GET['annee'];		
		}
}
//gestion de la filtre
if(!isset($_SESSION['filtre']) || preg_match('#index.php#',$_SERVER['PHP_SELF'])) {
		$_SESSION['filtre']    =   trim($params['CODE_FILTRE']);
} else {
		if(isset($_GET['filtre']) && $_GET['filtre'] != $_SESSION['filtre']) {
				$_SESSION['filtre'] = $_GET['filtre'];
		}
}
//gestion du style
if(!isset($_SESSION['style'])) {
	$_SESSION['style'] = trim($params['CODE_STYLE']);
} else {
	if(isset($_GET['style']) && $_GET['style'] != $_SESSION['style']) {
		$_SESSION['style'] = $_GET['style'];
	}
}
?>
