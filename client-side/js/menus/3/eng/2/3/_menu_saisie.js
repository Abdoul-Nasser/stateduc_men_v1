//Menu object creation
            oCMenu=new makeCM("oCMenu") //Making the menu object. Argument: menuname
            oCMenu.frames = 0
            //Menu properties   
            oCMenu.pxBetween =0
            oCMenu.fromTop=6
            oCMenu.rows=1 
            oCMenu.offlineRoot="" 
            oCMenu.onlineRoot="" 
            oCMenu.resizeCheck=1
            oCMenu.wait=300 
            oCMenu.fillImg="image/cm_fill.gif"
            oCMenu.zIndex=400
            oCMenu.menuPlacement=0  // permet de contr�ler (ou bloquer) la position � gauche pour �viter
                                    // dans notre cas, que le menu ne se superpose pas au logo
            
            //Background bar properties
            oCMenu.useBar=0         // 1 affiche la barre de menu, 0 la cache
            oCMenu.barWidth="100%"
            oCMenu.barHeight=50     // modifie la hauteur de la barre de menu
            oCMenu.barClass="clBar" // classe css pour la barre
            oCMenu.barX=0 
            oCMenu.barY=50
            oCMenu.barBorderX=0
            oCMenu.barBorderY=0
            oCMenu.barBorderClass=""
                                                    
            // Param�tres de cm_makeLevel(width, height, regClass, overClass, borderX, borderY, borderClass, rows, align, offsetX, offsetY, arrow, arrowWidth, arrowHeight, roundBorder)
            oCMenu.level[0]=new cm_makeLevel(90,21,"clT","clTover",1,1,"clB",0,"bottom",0,0,0,0,0);
            oCMenu.level[2]=new cm_makeLevel(110,22,"clS2","clS2over");
            oCMenu.level[3]=new cm_makeLevel(140,22);oCMenu.fromLeft=175;
            oCMenu.level[1]=new cm_makeLevel(102,22,"clS","clSover",1,1,"clB",0,"right",0,0,"client-side/image/menu_arrow.php",10,10);
oCMenu.makeMenu('1010','0','Home','accueil.php','','80');
oCMenu.makeMenu('1060','0','Select a Sector','saisie_donnees.php?val=choix_sys_saisie','','130');
oCMenu.makeMenu('1020','0','Select a School','saisie_donnees.php?val=choix_etablissement','','130');
oCMenu.makeMenu('1030','0','Questionnaires','questionnaire.php','','130');
oCMenu.makeMenu('1050','0','Add a School','saisie_donnees.php?val=new_etab&theme=101','','155');
oCMenu.makeMenu('1040','0','Quit','?val=logout','','42');
oCMenu.makeMenu('19602','1030','School Identification (Sup)','questionnaire.php?theme=19602&type_ent_stat=3','','200');
oCMenu.makeMenu('20202','1030','School Data AT A GLANCE','questionnaire.php?theme=20202&type_ent_stat=3','','200');
oCMenu.makeMenu('20302','1030','School Management','questionnaire.php?theme=20302&type_ent_stat=3','','200');
oCMenu.makeMenu('20402','1030','School Finance','questionnaire.php?theme=20402&type_ent_stat=3','','200');
oCMenu.makeMenu('20502','1030','Teaching-Learning Environment','questionnaire.php?theme=20502&type_ent_stat=3','','200');
oCMenu.makeMenu('20602','1030','Materials and Equipment','questionnaire.php?theme=20602&type_ent_stat=3','','200');
oCMenu.makeMenu('20802','1030','Attendance','#','','200');
oCMenu.makeMenu('20702','20802','Teachers attendance','questionnaire.php?theme=20702&type_ent_stat=3','','200');
oCMenu.makeMenu('20902','20802','Pupils monthly attendance','questionnaire.php?theme=20902&type_ent_stat=3','','200');
oCMenu.makeMenu('21002','1030','Headteachers feedback','questionnaire.php?theme=21002&type_ent_stat=3','','200');
oCMenu.makeMenu('21202','1030','Proposed Remed Act (Supervision)','questionnaire.php?theme=21202&type_ent_stat=3','','200');
oCMenu.makeMenu('21402','1030','Priority Remed Act Whole Sch Eval','questionnaire.php?theme=21402&type_ent_stat=3','','200');

//Leave this line - it constructs the menu
            oCMenu.construct();