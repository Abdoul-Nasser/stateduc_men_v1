//Menu object creation
            oCMenu=new makeCM("oCMenu") //Making the menu object. Argument: menuname
            oCMenu.frames = 0
            //Menu properties   
            oCMenu.pxBetween =0
            oCMenu.fromTop=6
            oCMenu.rows=1 
            oCMenu.offlineRoot="" 
            oCMenu.onlineRoot="" 
            oCMenu.resizeCheck=1
            oCMenu.wait=300 
            oCMenu.fillImg="image/cm_fill.gif"
            oCMenu.zIndex=400
            oCMenu.menuPlacement=0  // permet de contr�ler (ou bloquer) la position � gauche pour �viter
                                    // dans notre cas, que le menu ne se superpose pas au logo
            
            //Background bar properties
            oCMenu.useBar=0         // 1 affiche la barre de menu, 0 la cache
            oCMenu.barWidth="100%"
            oCMenu.barHeight=50     // modifie la hauteur de la barre de menu
            oCMenu.barClass="clBar" // classe css pour la barre
            oCMenu.barX=0 
            oCMenu.barY=50
            oCMenu.barBorderX=0
            oCMenu.barBorderY=0
            oCMenu.barBorderClass=""
                                                    
            // Param�tres de cm_makeLevel(width, height, regClass, overClass, borderX, borderY, borderClass, rows, align, offsetX, offsetY, arrow, arrowWidth, arrowHeight, roundBorder)
            oCMenu.level[0]=new cm_makeLevel(90,21,"clT","clTover",1,1,"clB",0,"bottom",0,0,0,0,0);
            oCMenu.level[2]=new cm_makeLevel(110,22,"clS2","clS2over");
            oCMenu.level[3]=new cm_makeLevel(140,22);oCMenu.fromLeft=175;
            oCMenu.level[1]=new cm_makeLevel(102,22,"clS","clSover",1,1,"clB",0,"right",0,0,"client-side/image/menu_arrow.php",10,10);
oCMenu.makeMenu('1010','0','Accueil','accueil.php','','80');
oCMenu.makeMenu('1060','0','Choisir un Secteur','saisie_donnees.php?val=choix_sys_saisie','','130');
oCMenu.makeMenu('1020','0','Choix Etablissement','saisie_donnees.php?val=choix_etablissement','','130');
oCMenu.makeMenu('1030','0','Questionnaires','questionnaire.php','','130');
oCMenu.makeMenu('1050','0','Ajout Etablissement','saisie_donnees.php?val=new_etab&theme=101','','155');
oCMenu.makeMenu('1040','0','Quitter','?val=logout','','42');
oCMenu.makeMenu('1604','1030','Identification et Pr�sentation','questionnaire.php?theme=1604','','180');
oCMenu.makeMenu('1704','1030','Mobilier','questionnaire.php?theme=1704','','180');
oCMenu.makeMenu('3004','1030','Effectif El�ves CAP','#','','180');
oCMenu.makeMenu('1804','3004','Option Commerciale Tertiaire','questionnaire.php?theme=1804','','180');
oCMenu.makeMenu('1904','3004','Option Batiment','questionnaire.php?theme=1904','','180');
oCMenu.makeMenu('2004','3004','Option Industrie','questionnaire.php?theme=2004','','180');
oCMenu.makeMenu('2104','3004','Option Artisanal','questionnaire.php?theme=2104','','180');
oCMenu.makeMenu('3104','1030','Effectif �l�ves BEP','#','','180');
oCMenu.makeMenu('2204','3104',' Option Tertiaire','questionnaire.php?theme=2204','','180');
oCMenu.makeMenu('2304','3104','Option Technicien de Surface','questionnaire.php?theme=2304','','180');
oCMenu.makeMenu('2404','3104','Option Industrie','questionnaire.php?theme=2404','','180');
oCMenu.makeMenu('2504','1030','Option commerciale','questionnaire.php?theme=2504','','180');
oCMenu.makeMenu('3204','1030','Effectif �l�ves Cycle Long','#','','180');
oCMenu.makeMenu('2604','3204','Technique Industrielle','questionnaire.php?theme=2604','','180');
oCMenu.makeMenu('2704','3204','Option Court Tertiaire','questionnaire.php?theme=2704','','180');
oCMenu.makeMenu('2804','3204','Cycle court Option Industrielle','questionnaire.php?theme=2804','','180');
oCMenu.makeMenu('4704','3204','BTP Option B�timent','questionnaire.php?theme=4704','','180');
oCMenu.makeMenu('4804','1030','Effectifs des OEV','questionnaire.php?theme=4804','','180');
oCMenu.makeMenu('4904','1030','Effectifs des �l�ves par �ge','questionnaire.php?theme=4904','','180');
oCMenu.makeMenu('2904','1030','R�sultats scolaires et personnel','questionnaire.php?theme=2904','','180');

//Leave this line - it constructs the menu
            oCMenu.construct();