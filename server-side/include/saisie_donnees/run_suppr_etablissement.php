<?php 
	set_time_limit(0);
	
	if( (trim($_GET['action_suppr']) <> '') ){
		
		$list_tables 	= array();
		$list_tables 	= 	get_dico_tables_by_sector($_SESSION['secteur']) ;	
		$GLOBALS['sup_etab_success'] = true;
		$GLOBALS['etab_enfant_exist'] = false;
				
		if( ($_GET['action_suppr'] == 'suppr_etab') ){
			$l_tabms_del = $list_tables;
			krsort($l_tabms_del);
			
			if( isset($_SESSION['code_etab']) and (trim($_SESSION['code_etab']) <> '') ){
				$conn = $GLOBALS['conn'];
				if(isset($GLOBALS['PARAM']['CODE_ETABLISSEMENT_PARENT']) && $GLOBALS['PARAM']['CODE_ETABLISSEMENT_PARENT'] <> ''){
					$req_etab_enfant   	= "SELECT COUNT(".$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].") FROM ".$GLOBALS['PARAM']['ETABLISSEMENT']." WHERE ".$GLOBALS['PARAM']['CODE_ETABLISSEMENT_PARENT']." = ".$_SESSION['code_etab'];
					$nb_etab_enfant = $conn->GetOne($req_etab_enfant);
					if($nb_etab_enfant > 0){
						$GLOBALS['etab_enfant_exist'] = true;
						$GLOBALS['sup_etab_success'] = false;
					}
				}
				if(!$GLOBALS['etab_enfant_exist']){	
					if( isset($_SESSION['hierarchie_regroup']) )	unset($_SESSION['hierarchie_regroup']);
					if( isset($infos_etab) )	unset($infos_etab);
					foreach ($l_tabms_del as $table){ 
						$sql=	'DELETE FROM '.$table.' WHERE '.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].' = '.$_SESSION['code_etab'];
						if ($conn->Execute($sql) === false) {
							$GLOBALS['sup_etab_success'] = false;
							print("<script type=\"text/javascript\">\n");
							print("\t <!-- \n");
							print("alert(\"".recherche_libelle_page('SupDataEtErr')." ".$table."\"); \n");
							print("\t //--> \n");
							print("</script>\n");
						}
					}
				}
			}
		}
		elseif( ($_GET['action_suppr'] == 'suppr_donnees') && ($_GET['annee_suppr'] <> '') ){
			$l_tabms_del = $list_tables;
			krsort($l_tabms_del);
			
			foreach($l_tabms_del as $i => $table){
				if(exist_champ_in_table($GLOBALS['PARAM']['CODE_ETABLISSEMENT'], $table) && exist_champ_in_table($GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_ANNEE'], $table)){
					$req_del = 'DELETE FROM '.$table.' WHERE '.$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_ANNEE'].'='.$_GET['annee_suppr'].
							   ' AND '.$GLOBALS['PARAM']['CODE_ETABLISSEMENT'].'='.$_SESSION['code_etab'];
					if($GLOBALS['conn']->Execute($req_del) === false) {
						$GLOBALS['sup_etab_success'] = false;
						print("<script type=\"text/javascript\">\n");
						print("\t <!-- \n");
						print("alert(\"".recherche_libelle_page('SupDataEtErr')." ".$table."\"); \n");
						print("\t //--> \n");
						print("</script>\n");
					}
				}
			}
		}
		if($GLOBALS['sup_etab_success'] == true){
				////////// alert sup successs
				print("<script type=\"text/javascript\">\n");
				print("\t <!-- \n");
				print("alert(\"".recherche_libelle_page('SupEtOK')."\"); \n");
				print("\t //--> \n");
				print("</script>\n");
		}
		else{
			if($GLOBALS['etab_enfant_exist']){
				print("<script type=\"text/javascript\">\n");
				print("\t <!-- \n");
				print("alert(\"".recherche_libelle_page('SupEtabEnfant')."\"); \n");
				print("\t //--> \n");
				print("</script>\n");
			}else{
				print("<script type=\"text/javascript\">\n");
				print("\t <!-- \n");
				print("alert(\"".recherche_libelle_page('SupEtErr')."\"); \n");
				print("\t //--> \n");
				print("</script>\n");
			}
		}
	}
?>


