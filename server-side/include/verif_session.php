<?php // Ce fichier permet de s�curiser l'acc�s aux donn�es. 
// Il faut necessairement s'�tre authentifi� pour le passer.
// 
// Pour s�curiser un fichier il suffira donc d'inclure en d�but de script ce fichier.

//session_cache_expire (60);/* Configure le d�lai d'expiration � 60 minutes */

if (!$_SESSION['valide'])
{
	header('Location: '.$SISED_AURL.'index.php');
	exit();
}

?>
