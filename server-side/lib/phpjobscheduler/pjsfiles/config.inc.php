<?php
// ---------------------------------------------------------
 $app_name = "phpJobScheduler";
 $phpJobScheduler_version = "3.9";
// ---------------------------------------------------------

define('DBHOST', 'localhost');// database host address - localhost is usually fine
define('DBPORT', '3306');
define('DBNAME', 'phpjobscheduler');// database name - must already exist
define('DBUSER', 'phpjobscheduler');// database username - must already exist
define('DBPASS', 'phpjobscheduler');// database password for above username

define('DEBUG', false);// set to false when done testing