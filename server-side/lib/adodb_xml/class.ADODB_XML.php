<?php require("class.xml.php"); ?>
<?php
/**
* Class of Manipulation of files XML and SGBD
*
* @author    			Olavo Alexandrino <oalexandrino@yahoo.com.br> - 2004
* @originalAuthor		Ricardo Costa <ricardo.community@globo.com>   - 2002
* @based				MySQL to XML - XML to MySQL - <http://www.phpclasses.org/browse/package/782.html>
* @require				Class XMLFile <Olavo Alexandrino version> - <orignal version: http://www.phpclasses.org/browse/package/79.html>
		- function commentary to strtoupper of the methods:
			1. add_attribute
			2. set_name
* @require				ADOdb Database Library for PHP <http://php.weblogs.com/adodb#downloads>
*/

class ADODB_XML 
{
	/**
	*	Object representation:  File XML
	*
	*	@type		objcet
	*	@access		public
	*/
	var $xml = null; 

	/**
	*	Creating the members
	*
	*	@param		string		Version of file XML
	*	@param		string		Codification to be used	
	*	@access		public
	*/
	function ADODB_XML($version = "", $encoding = "") 
	{
	  $this->xml = new XMLFile($version, $encoding);
	}
	
	/**
	*	It converts Table of the SGBD into file XML
	*
	*	@param		object 			Connection of ADOdb Database Library	
	*	@param		string			Query SQL			
	*	@param		string			Name of existing file XML
	*	@access		public
	*	@return 	void	
	*/	
	function ConvertToXML($dbConnection, $strSQL, $fields, $filename) 
	{
	  $dbConnection->SetFetchMode(ADODB_FETCH_ASSOC);
	  $rs = $dbConnection->Execute($strSQL);
	  
	  $this->xml->create_root();
	  $this->xml->roottag->name = "ROOT";
	  //print_r($rs->fields);return;
	  while(!$rs->EOF)
	  {
		 $this->xml->roottag->add_subtag("ROW", array());
		 $tag = &$this->xml->roottag->curtag;
		 
		 for ($i = 0; $i < $rs->_numOfFields ; $i++)
		 {
			list($field, $value) = each($rs->fields);
			$fieldOk = trim($fields[$i]);		 
			$tag->add_subtag($fieldOk); //echo $field."\n\n";
			$tag->curtag->cdata = $value;
		 }	  
	  
		 $rs->moveNext();
	  }
	   
	  $xml_file = fopen($filename, "w" );   
	  $this->xml->write_file_handle( $xml_file );
	  $xml_data = file_get_contents($filename);
	  // replace '&' followed by a bunch of letters, numbers
	  // and underscores and an equal sign with &amp;
	  $xml_data = preg_replace("|&([^;]+?)[\s&]|","&amp;$1 ",$xml_data);
	  file_put_contents($filename, $xml_data);
	}
	
	
	/**
	*	It inserts XML in table of the SGBD
	*
	*	@param		object 			Connection of ADOdb Database Library	
	*	@param		string			Name of to be created file XML	
	*	@param		string			Table of BD		
	*	@access		public
	*	@return 	void	
	*/	
	function InsertIntoDB($dbConnection, $filename, $tablename) 
	{
	
	  $xml_file = fopen($filename, "r"); 
	  $this->xml->read_file_handle($xml_file);
	
	  $numRows = $this->xml->roottag->num_subtags();
    
		$meta_types_integer 	= array('L', 'N', 'I', 'R');
		$meta_types_date		= array('D', 'T');
		
		$fiedsType = array();
		$all_champs_table = $dbConnection->MetaColumns($tablename);
		$primaryKeys = MetaPrimaryKeys($tablename);
		if (!$primaryKeys || count($primaryKeys) == 0) {
			$primaryKeys = $dbConnection->MetaPrimaryKeys($tablename); 
		} //echo "\n".$tablename."\n"; print_r($primaryKeys);
		foreach( $all_champs_table as $champ ) {
			$meta_type 	= $champ->type;
			//echo "<br>$nom_champ, $table, ".$type_champ.", $meta_type<br>";
			if(in_array($meta_type , $meta_types_integer)){
			$fiedsType[$champ->name] = "I";
			}elseif(in_array($meta_type, $meta_types_date)){
				$fiedsType[$champ->name] = "D";
			}else{
				$fiedsType[$champ->name] = "S";
		}	}	  
		
		
	  for ($i = 0; $i < $numRows; $i++) 
	  {
		   $arrFields = null;
		   $arrValues = null; 
		   $arrWhere = null;
		   $arrUpdateFields = null;
	
		   $row = $this->xml->roottag->tags[$i];
		   $numFields = $row->num_subtags();
	
		   for ($ii = 0; $ii < $numFields; $ii++) 
		   {
				$field = $row->tags[$ii];
				$value = "";
				if ($fiedsType[$field->name] == "I") {
					$value = $field->cdata;
					if ($value == "") {
						continue;
					}
				} else {
					$value = $dbConnection->qstr($field->cdata);
				}
				$arrFields[] = $field->name;
				$arrValues[] = $value;
			  	if (in_array($field->name, $primaryKeys)) {
					$arrWhere[] = $field->name."=".$value;
				} else {
					$arrUpdateFields[] = $field->name."=".$value;
				}
		   }
	
		   $fields = join($arrFields, ", ");
		   $values = join($arrValues, ", ");
		   $where = join($arrWhere, " AND ");
		   $updateFields = join($arrUpdateFields, ", ");
	
		   $strSQL = "INSERT INTO $tablename ($fields) VALUES ($values)";
		   if ($dbConnection->Execute($strSQL) === false) {  //if ($tablename == 'SCHOOL') echo $strSQL."\n\n"; 
				if ($tablename != $GLOBALS['PARAM']['ETABLISSEMENT_REGROUPEMENT']) {
					$strSQL = "UPDATE " . $tablename . " SET " . $updateFields. " WHERE " .$where;
					//echo $strSQL."\n\n";
					if ($dbConnection->Execute($strSQL) === false) {
						//return false;
					}
				}
				//return true;
		   }
	  } 
   	  return true;
		  
	}
   
}// end class
?>