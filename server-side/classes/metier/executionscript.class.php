<?php /**
     * Classe executionscript permet d'�xecuter des requ�tes (Etat bas� sur plusieurs tables)
     * Ces requ�tes interm�diares permettent de cr�er une table temporraire 
     * qui permettra de stocker les donn�es provenant d'autres tables
	 * @package default
     * @access  public
     * @author Alassane, Tour�, Yacine
     * @version 1.4	 
    */ 
class executionscript {

		/**
		* Attribut : code_systeme
		* Permet de se positionner sur le secteur courant
		* @var numeric 
		* @access public
		*/   
		public $code_systeme;
		/**
		* Attribut : id_report
		* reference du report
		* @var numeric 
		* @access public
		*/       	
		public $id_report;
        
        /**
		* Attribut : conn
		* Variable de connexion � la base de donn�es
		* @var ADODB.connection 
		* @access public
		*/   
		public $conn;
		
        /**
		* Attribut : queries
		* Tableau contenant les requ�tes � ex�cuter
		* @var array 
		* @access public
		*/  
        public $queries   =   array();        
        /**
		* Constructeur de la classe: chargement de la liste des requ�tes,
		* ex�cution des requ�te
		* @access public
		* @param tableau param tableau contenant l'identifiant de l'�tat, le syst�me d'enseignement courant et la connxion courante
		*/
		public function __construct($param){
            // Initialisation de certains attributs de la classe
            $this->id_report        =   $param["ID_REPORT"];
            $this->code_systeme     =   $param["ID_SYSTEME"];
            $this->conn             =   $GLOBALS['conn'];
            
            // R�cup�ration de la s�rie de requ�te sql associ�s au report
            $this->get_query();
            
            // Exection de la s�rie des requ�te sql
            $this->run_query();
        }
        
		/**
		* Activation de la connexion
		* @access private
		* 
		*/
		public function __wakeup(){
        	$this->conn     =   $GLOBALS['conn'];       
        }
        
		/**
		* Construction et ex�cution de la requ�te permettant de charger la liste des requ�tes interm�diaires li�es � l'�tat
		* @access public
		* 
		*/
		public function get_query(){            
            // Requete de selection de la liste des requetes sql associ�s au report
            $requete    ='SELECT ID_QUERY, SQL_QUERY, ORDRE_EXECUTION,TYPE_QUERY FROM DICO_QUERY WHERE ID='.$this->id_report;
            $requete   .=' ORDER BY ORDRE_EXECUTION ';
            
            // Gestion des erreurs lors de l'ex�cution de la requ�te sql
            try{            
                $this->queries = $GLOBALS['conn_dico']->GetAll($requete);   
            }        
            catch (Exception $e){
                $erreur=new erreur_manager($e,$requete);
            }
             
        }
        
        /**
		* Ex�cution des requ�tes en tenant compte du type de la requ�te
		* @access public
		* 
		*/
		public function run_query(){
            //Instanciation d'un dictionnaire
            $dict = NewDataDictionary($this->conn);

            // Ex�cution de la s�rie des requ�tes
            
            if (is_array($this->queries)){
                foreach ($this->queries as $query){
                    // parcours de chaque requete et ex�cution
                    if($query['TYPE_QUERY']==1){
                        if (trim($query['SQL_QUERY'])<>''){
                            // gestion des erreurs d'ex�cution des requete
                            unset($quer);
                            $quer[] = $query['SQL_QUERY'];
                            if ($dict->ExecuteSQLArray($quer)<> 2)   
                                echo '<br> error in the query :'.$query['SQL_QUERY'].'<br>';                      
                            }
                    }else{
                    $sql = str_replace('$code_annee',$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_ANNEE'].'='.$_SESSION['annee'],$query['SQL_QUERY']);
                    $sql = str_replace('$code_filtre',$GLOBALS['PARAM']['CODE'].'_'.$GLOBALS['PARAM']['TYPE_FILTRE'].'='.$_SESSION['filtre'],$sql);
					//echo '<br>'.$sql.'<br>';
                    if (trim($sql)<>''){
                            // gestion des erreurs d'ex�cution des requete
                            try{
                                if($this->conn->execute($sql)==false)   
                                    print '<BR>error inserting: '.$this->conn->ErrorMsg().'<BR>';                        
                            }
                            catch (Exception $e){
                                $erreur=new erreur_manager($e,$sql);
                            }
                        }
                    }
                }
            }
        }
}
?>
