<?php /** 
		* Classe qui permet l'acc�s au contenu des th�mes configur�s
		* Elle permet la r�cup�ration des th�mes existants et de leur ordonnancement,
		* Mais s'occupe �galement du charger la classe M�tier selon le th�me en cours de traitement,
		* @access public
	*/	
	class theme_manager {
    
		
		/**
		 * Attribut : id
		 * 	Correspond au ID du th�me en cours
		 * @var numeric
		 * @access public
		 */   
		public $id;
    	
		
		/**
		 * Attribut : pos_thm
		 * 	Position Th�me courant
		 */  
		 public $pos_thm;
    	
		/**
		 * Attribut : conn
		 * 	Connexion � la Base
		 * @var array
		 * @access public
		 */     
		public $conn;
    	
		/**
		 * Attribut : list
		 * 	Stocke la liste des th�mes existants dans la Config
		 * @var array
		 * @access public
		 */   
		public $list;
    	
		/**
		* Attribut : id_theme_systeme
		* 	Correspond au th�me configur� pour le syst�me
		* @var numeric
		* @access public
		*/   
		public $id_theme_systeme;
    	
		/**
		* Attribut : classe
		* 	le nom de la classe M�tier utilis�e par le th�me
		* @var string
		* @access public
		*/   
		public $classe;

		/**
		 * METHODE :  __construct()
		 * 	Constructeur de la classe :
		 * @access public
		 */ 
		public function __construct($appart="") {
	
			$this->conn = $GLOBALS['conn'];
		
			$this->charger_theme($appart);
		
			$this->set_theme_courant();
			$this->set_classe();
	
		}

		/**
		 * METHODE :  recherche_theme_prec()
		 * 	Permet de rechercher � partir de la liste des th�mes
		 *	celui qui pr�c�de le th�me en cours
		 * @access public
		 */ 
		public function recherche_theme_prec() {

        // Retourne le th�me pr�c�dent le theme_courant � partir du tableau des th�mes tri�s
        foreach ($this->list as $k => $theme) {

        if ($this->id_theme_systeme == $theme['ID_THEME_SYSTEME']) {

                if(isset($this->list[$k-1])) {

                    if($this->list[$k-1]['ID_TYPE_THEME'] != 8) {

                        return $this->list[$k-1]['ID_THEME_SYSTEME'];

                    } else {

                        if(isset($this->list[$k-2])) {

                            return $this->list[$k-2]['ID_THEME_SYSTEME'];

                        } else {

                            return false;

                        }

                    }

                } else {

                    return false;

                }

            }

        }

    }

		/**
		 * METHODE :  recherche_theme_suiv()
		 * 	Permet de rechercher � partir de la liste des th�mes
		 *	celui qui succ�de au th�me en cours
		 * @access public
		 */ 
		public function recherche_theme_suiv() {

        // Retourne le th�me suivant le theme_courant � partir du tableau des th�mes tri�s
        foreach ($this->list as $k => $theme) {

            if ($this->id_theme_systeme == $theme['ID_THEME_SYSTEME']) {

                if(isset($this->list[$k+1])) {

                    if($this->list[$k+1]['ID_TYPE_THEME'] != 8) {

                        return $this->list[$k+1]['ID_THEME_SYSTEME'];

                    } else {

                        if(isset($this->list[$k+2])) {

                            return $this->list[$k+2]['ID_THEME_SYSTEME'];

                        } else {

                            return false;

                        }

                    }

                } else {

                    return false;

                }

            }

        }

    }

    	/**
		 * METHODE :  charger_theme()
		 * 	Permet charger les th�mes configur�s
		 * @access public
		 */
		public function charger_theme($appart="") {

       if (!isset($langue) and (isset($_SESSION['langue'])) ){
            $langue = $_SESSION['langue'];
       }
       if (!isset($secteur) and (isset($_SESSION['secteur']))){
            $secteur = $_SESSION['secteur'];
       }
        // Lecture des infos du theme dans la table DICO_THEME du dico
        // Ajout Alassane
        //echo 'pas�e alasco <br>';
       if (!isset($langue) || !isset($secteur)) {
				$requete = "SELECT * FROM PARAM_DEFAUT;";
				
				// Traitement Erreur Cas : GetAll / GetRow
				try {
						$paramdefaut = $GLOBALS['conn_dico']->GetRow($requete);
						if(!is_array($paramdefaut)){                    
								throw new Exception('ERR_SQL');  
						} 
						if (!isset($langue)) $langue    =   trim($paramdefaut['CODE_LANGUE']);
						if (!isset($secteur)) $secteur  =   trim($paramdefaut['CODE_SECTEUR']);
				}
				catch(Exception $e){
						$erreur = new erreur_manager($e,$requete);
				}
				// Fin Traitement Erreur Cas : GetAll / GetRow		
        }
        
        // Fin ajout alassane
		if($appart <> "") $critere_appart = " AND  D_T_S.APPARTENANCE=$appart ";
		else $critere_appart = "";
		$requete = "SELECT D_T_S.ID_THEME_SYSTEME, D_T.ID_TYPE_THEME, D_T_S.ID, D_T_S.PERE, D_T_S.PRECEDENT, D_T_S.TAILLE_MENU, D_TRAD.LIBELLE
					FROM DICO_THEME_SYSTEME AS D_T_S, DICO_TRADUCTION AS D_TRAD, DICO_THEME AS D_T
					WHERE D_T.ID = D_TRAD.CODE_NOMENCLATURE
					AND D_T.ID = D_T_S.ID
					AND D_TRAD.NOM_TABLE='DICO_THEME'
					AND  D_T_S.ID_SYSTEME=".$secteur.$critere_appart."
					AND D_TRAD.CODE_LANGUE='".$langue."';";
        // r�cup�ration du r�sultat dans un tableau       
				// Traitement Erreur Cas : GetAll / GetRow
		//echo $requete.'<br>';
        try {
						$result_theme	= $GLOBALS['conn_dico']->GetAll($requete);
						//print_r($result_theme);
						if(!is_array($result_theme)){                    
								throw new Exception('ERR_SQL');  
						}
						// tri de la table selon les precedences 
						$this->list = $this->tri_list($result_theme);	                                          
				}
				catch(Exception $e){
						$erreur = new erreur_manager($e,$requete);
				}
              
    }

		/**
		 * METHODE :  set_theme_courant()
		 * 	permet de placer convenablement le th�me courant
		 * @access public
		 */
		public function set_theme_courant() {
    
			if ($_GET['val'] == 'new_etab') { // Lors de la cr�ation d'un nouvel �tab
				$this->id_theme_systeme  = $this->recherche_theme_def();
			}elseif (isset($_GET['theme_frame'])) { //si $_GET['theme'] existe alors le theme courant est $_GET['theme']
				$this->id_theme_systeme    = $_GET['theme_frame'];
			}elseif (isset($_GET['theme'])) { //si $_GET['theme'] existe alors le theme courant est $_GET['theme']
				$this->id_theme_systeme  = $_GET['theme'];
			}else {
				$this->id_theme_systeme  = $this->recherche_theme_def();
			}
			foreach($this->list as $ord_thm => $thm) {
				if($thm['ID_THEME_SYSTEME'] == $this->id_theme_systeme) {
					$this->id = $thm['ID'];
					$this->pos_thm 	= $ord_thm;
					break;
				}
			}
		}

		/**
		 * METHODE :  set_theme_courant(id_theme_systeme)
		 * 	permet r�cup�rer la variable ID su th�me courant
		 *	� partir du th�me syst�me
		 * @access public
		 * @param numeric id_theme_systeme : le th�me syst�me en cours
		 */
		public function set_id_from_id_thm_sys($id_theme_systeme){
        foreach($this->list as $l) {

            if($l['ID_THEME_SYSTEME'] == $id_theme_systeme) {

                $this->id = $l['ID'];
                break;

            }
        }
    }

		/**
		 * METHODE :  set_classe()
		 * 	permet r�cup�rer la classe M�tier 
		 *	� partir du ID th�me courant
		 * @access public
		 */
		public function set_classe() {
        
        $requete = 'SELECT CLASSE 
                    FROM DICO_THEME 
                    WHERE ID='.$this->id;                    
       
					
        // r�cup�ration du r�sultat dans un tableau
				// Traitement Erreur Cas : GetAll / GetRow
				try {
						$res	= $GLOBALS['conn_dico']->GetAll($requete);
						if(!is_array($res)){                    
								throw new Exception('ERR_SQL');  
						} 
						$this->classe = $res[0]['CLASSE'];									
				}
				catch(Exception $e){
						//  theme manager est appel� avant session_start() s'il y'un affichage cela risque de 
						/// de provoquer une erreur comme : Cannot send session cookie - headers already sent by 
						//$erreur = new erreur_manager($e,$requete);
				}
				// Fin Traitement Erreur Cas : GetAll / GetRow
    }

		/**
		 * METHODE :  tri_list(dico, racine=1030)
		 * 	Permet d'effectuer le tri des th�mes conform�ment � 
		 *	l'ordonancement d�fini dans la configuration
		 * @access public
		 * @param array dico : Lot des th�mes issus de la Base
		 * @param numeric racine : le Menu de d�part qui par d�faut = Questionnaire
		 */
		public function tri_list(&$dico, $racine=1030) {

        $noeuds = array();
        $listes = array();
		//echo '<pre>';
		//print_r($dico);
        //on recherche les noeuds et on d�finit le premier �l�ment
        foreach($dico as $d) {

            //on trouve l'id du premier �l�ment
            if($d['PERE'] == $racine && $d['PRECEDENT'] == 0) {

                $first = $d;

            }

            //si PRECEDENT est �gal � 0 c'est un noeud
            if($d['PRECEDENT'] == 0) {

                array_push($noeuds, $d);

            }

        }

        //on construit les listes commen�ant par des noeuds
        foreach($noeuds as $n) {

            //si la liste pour le noeud n'existe pas alors on la cr�e
            if(!isset($listes[$n['PERE']])) {

                $listes[$n['PERE']] = array();

            }

            array_push($listes[$n['PERE']], $n);
            $curel = $n;

            while($nextel = $this->tri_get_next_el($dico, $curel)) {

                array_push($listes[$n['PERE']], $nextel);
                $curel = $nextel;

            }

        }

        //on construit le dico ordonn� r�cursivement
        $dico = $this->tri_build_ordered_dico($first, $listes);

        return $dico;

    }

		/**
		 * METHODE :  tri_build_ordered_dico(first, listes)
		 * 	Permet � partir du tableau listes, de r�cup�rer
		 *	les th�mes fils associ�s au th�me $first pour la constitution
		 *	du Menu << Questionannaires >>
		 * @access public
		 * @param array listes : Tableau de th�mes
		 * @param numeric first : th�me ayant des fils pour le Menu
		 */
		public function tri_build_ordered_dico($first, $listes) {

        $result = array();

        if(isset($listes[$first['PERE']])) {

            //on boucle sur la liste de l'�l�ment premier
            foreach($listes[$first['PERE']] as $l) {

                //on ajoute les �l�ments
                array_push($result, $l);

                //et on y rajoute � chaque fois les occurences de la liste correspondant � l'ID courant cad ses enfants
                if(isset($listes[$l['ID_THEME_SYSTEME']])) {

                    $result = array_merge($result, $this->tri_build_ordered_dico($listes[$l['ID_THEME_SYSTEME']][0], $listes));

                }

            }

        }

        return $result;

    }

		/**
		 * METHODE :  tri_get_next_el(dico, el)
		 * 	Permet � partir du tableau dico, de r�cup�rer
		 *	le qui succ�de � l'�l�ment el 
		 * @access public
		 * @param array dico : Tableau de th�mes
		 * @param numeric el : un th�me donn�
		 */
		public function tri_get_next_el(&$dico, $el) {

        foreach($dico as $i=>$d) {

            if($d['PRECEDENT'] == $el['ID_THEME_SYSTEME'] && $d['PERE'] == $el['PERE']) {

                //on d�truit l'�l�ment retenu afin de ne pas avoir a boucler sur des �l�ments d�ja dispatch�
                unset($dico[$i]);

                return $d;

            }

        }

        return false;

    }

		/**
		 * METHODE :  recherche_theme_def()
		 * 	Retrouve le th�me par d�faut, le tout premier de la Liste 
		 * @access public
		 
		 */
		public function recherche_theme_def() {

        return $this->list[0]['ID_THEME_SYSTEME'];

    }
		
		/**
		 * METHODE :  get_lib_long_theme (id_theme_sys, langue)
		 * 	Retrouve le libell� long du th�me syst�me id_theme_sys
		 *	pour la langue en param�tre
		 * @access public
		 * @param array id_theme_sys : un th�me syst�me donn�
		 * @param string langue : langue choisie
		 */
		public function get_lib_long_theme ($id_theme_sys, $langue){
				$requete        = "SELECT LIBELLE
														FROM DICO_TRADUCTION 
														WHERE CODE_NOMENCLATURE=".$id_theme_sys." AND CODE_LANGUE='".$langue."'
														AND NOM_TABLE='DICO_THEME_LIB_LONG'";
				
				// Traitement Erreur Cas : Execute / GetOne
				try {            
						$lib = $GLOBALS['conn_dico']->GetOne($requete);
						if($lib ===false){                
								 throw new Exception('ERR_SQL');   
						}
						return $lib; 								 
				}
				catch (Exception $e) {
						//  theme manager est appel� avant session_start() s'il y'un affichage cela risque de 
						/// de provoquer une erreur comme : Cannot send session cookie - headers already sent by 
						// $erreur = new erreur_manager($e,$requete);
				}        
				// Fin Traitement Erreur Cas : Execute / GetOne
		}
}

?>